# Keep your head above water
A game made for the WonderJam in October 2019. The WonderJam is a GameJam organized in "Université du Québec à Chicoutimi" which last 48 hours. <br/>
The theme was: "Climate catastrophe on the horizon of 2119". <br/>
Game types we had to respect were: 
- Arcade
- Race
- Multiplayer

# Game description
Keep your head above water is a 2D platformer where the players have to be the first to reach the top of a building before they get caught up by the water. <br/>
Different objects are placed in the level and the players can choose to use them to cooperate or to fight each other. In all case, only one will be able to be evacuated by the helicopter. <br/>
All players have access too to a fishing rod which can be used to caught other players.

# Tools used
The game has been made with Unity 2019. We used the Rewired extension for the controls.

# Team
For the GameJam, we were a team of 6:
- Axel Bertrand
- Laura Bouzeraud
- Baptiste Rey
- Bertrand Gilbert-Collet
- Clément Cariou
- Benjamin Perez

# My contribution
At the start of the GameJam I didn't know much about using Unity for making games. So I learned how to use it during the GameJam.  <br/>
My job mainly consisted of doing all the menus in the game, from the starting menu to the victory panel. I integrated the different sounds and musics to the game too.